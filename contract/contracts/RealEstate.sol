// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/utils/Counters.sol";


contract RealEstate is ERC721  {
    
    //Used to generate new id for RealEstate each time
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;
    
    
    //Store price asked by the owner tokenID VS price
    mapping (uint256 => uint256) _prices;
    
    //Return if token is in sell
    mapping (uint256 => bool) _inSell;
    
    address payable _ownerRealestateAgency;
    uint256 _commissionBalance = 0;
    uint256 _commissionRate = 5.0;
    

   constructor() public ERC721("RealEstate", "RET") {
       _ownerRealestateAgency = msg.sender;
   }
   
   
   function getContractOwner() public view returns (address){
       return _ownerRealestateAgency;
   }
   
   //For my case dataReference contains data of property + link to img url
    function declareSell(string memory dataReference,uint256 weiPrice ) public returns (uint256) {
        _tokenIds.increment();
        uint256 realEstateID = _tokenIds.current();
        
        _mint(msg.sender, realEstateID);
        _prices[realEstateID] = weiPrice;
        _setTokenURI(realEstateID, dataReference);
        
        _inSell[realEstateID] = true;
        

        return realEstateID;
    }
    
    function updateURIRef(uint256 realEstateID,string memory dataReference, uint256 weiPrice) public{
        require(msg.sender == ERC721.ownerOf(realEstateID) , 'You need to be owner of token to toggle sell prop');
         _setTokenURI(realEstateID, dataReference);
         _prices[realEstateID] = weiPrice;
    }
    
    
    function buyRealEstate(uint256 realEstateID) public payable{
        require(msg.value == _prices[realEstateID] , 'Amount sent need to be equal to price');
        require(msg.sender != ownerOf(realEstateID) , 'Impossible to sell realestate to yourself');
        require(isInSell(realEstateID) , 'Realestate not in sale');
        
        
        //We save owner before that's change
        address payable oldOwner = payable(ownerOf(realEstateID));
        
        //We transfert token
        safeTransferFrom(ownerOf(realEstateID),msg.sender,realEstateID);
        
        //We transfer ETH to seller
        uint256 totalPrice = _prices[realEstateID];
        uint256 commission  = totalPrice / 100 * _commissionRate;
        uint256 toReverve = totalPrice - commission;
        
        _commissionBalance = _commissionBalance + commission;
        oldOwner.transfer(toReverve);
        
        _inSell[realEstateID] = false;
    }
      
    function getAllTokens() public view returns (uint256 [] memory) {
        uint256 tokenQuantity = totalSupply();
        
        uint256[] memory allTokens = new uint256[](tokenQuantity) ;
        
        for(uint256 i = 0; i < tokenQuantity; i++){
          allTokens[i] =  tokenByIndex(i);
        }
        return allTokens;
    }
    
    function isInSell(uint256 realEstateID) public view returns (bool){
        return _inSell[realEstateID];
    }
    
    function setInSell(uint256 realEstateID,bool inSell) public {
        require(msg.sender == ERC721.ownerOf(realEstateID) , 'You need to be owner of token to toggle sell prop');
        _inSell[realEstateID] = inSell;
    }
    
    function getPrice(uint256 realEstateID) public view returns (uint256){
        return _prices[realEstateID];
    }
    
    
    
    
    
    
    
    
    function getComTreasure() public view returns (uint256){
        return _commissionBalance;
    }
    
    function getComRate() public view returns (uint256){
        return _commissionRate;
    }
    
    
    function changeCommission(uint256 commissionRatePercent) public {
        require(msg.sender == _ownerRealestateAgency , 'You need to be owner of contract do change Commission');
        _commissionRate = commissionRatePercent;
    }
    

    function withdrawCommission() public {
        require(msg.sender == _ownerRealestateAgency , 'You need to be owner of contract do withdraw Commission');
        _ownerRealestateAgency.transfer(_commissionBalance);
        _commissionBalance = 0;
    }
    
    
    // ============================== DEV Tools ===========================================
    
    //This is for dev, used to transform you to administrator
    function devToolMakeMeAdmin() public {

       _ownerRealestateAgency = msg.sender;

    }
      
    

   
    
    // ============================ ovverided =============================================
    
    //We always pre approve transaction one they has been put in sell, this is ovverided
    function _isApprovedOrOwner(address , uint256 ) internal override view returns (bool) {
        return true;
    }
    


}
