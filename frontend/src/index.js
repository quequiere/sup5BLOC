import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Home from './views/Home';
import store from './redux/store';
import { Provider } from 'react-redux';
import * as serviceWorker from './serviceWorker';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import {
  Container
} from "reactstrap"

import YourProperties from "views/YourProperties"
import Buy from "views/Buy"
import Admin from "views/Admin"

import 'bootstrap/dist/css/bootstrap.css';

ReactDOM.render(
  <React.Fragment>
    <Provider store={store}>

      <Router>
        <Home />
        <Switch>
          <Container>
            <Route exact path="/"></Route>
            <Route exact path="/buy"><Buy/></Route>
            <Route exact path="/admin"><Admin/></Route>
            <Route exact path="/myprops"><YourProperties /></Route>
          </Container>
        </Switch>
      </Router>

    </Provider>
  </React.Fragment>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
