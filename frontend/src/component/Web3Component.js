import React from 'react';
import { connect } from "react-redux"


import { updatedBalance, updateContractName, loadAddress, loadAllTokens, loadAdminData } from "redux/actions"

import contractData from "assets/RealEstate.json"


import "assets/Metamask.css"
import { Button, Input, Row, Col, Container } from "reactstrap"

const Web3 = require('web3');

class Web3Component extends React.Component {

  state = {
    isConnected: false,
    isForced: false,
    contractAddress: '0x73c6Fa2cC2B2998f47569418AEd6fb5115D44dEE',
  }


  render() {
    if (!this.state.isConnected && !this.state.isForced) return this.renderNotConnected()
    else return null
  }


  inputAddress() {
    this.setState({
      contractAddress: this.state.inputContractAdd
    })
  }

  renderNotConnected() {
    return <div>
      <div className="metamaskAvert text-center">
        <Container>
          <Row>
            <Col>
              <h1>Please connect to metamask to access to this website </h1>
            </Col>
          </Row>
          <Row>
            <Col>
              <Button onClick={this.force.bind(this)} color={"danger"}>Force access</Button>
            </Col>
          </Row>
        </Container>
      </div>
    </div>

  }

  force() {
    this.setState({
      isForced: true
    })
  }


  componentDidMount() {
    window.web3 = new Web3(window.ethereum)
    window.ethereum.enable()

    const instance = this.props
    const thisRef = this


    setInterval(async function () {

      let data = await window.web3.eth.getAccounts()

      if (data.length > 0) {

        thisRef.setState({
          isConnected: true
        })

      } else {
        thisRef.setState({
          isConnected: false
        })
        return
      }



      if (!window.REContract) {
        try {
          window.REContract = new window.web3.eth.Contract(contractData.abi, thisRef.state.contractAddress)

          window.REContract.methods.name().call().catch(e=>{alert("Failed to connect to contract, are you on connected to rinkeby network ?")})
          return
        } catch (e) {

        }
      }



      instance.loadAllTokens()
      instance.updatedBalance()
      instance.updateContractName()
      instance.loadAddress()
      instance.loadAdminData()


    }, 1000);
  }




}
const mapStateToProps = state => {
  return {

  }
}

export default connect(mapStateToProps, { updatedBalance, updateContractName, loadAddress, loadAllTokens, loadAdminData })(
  Web3Component
)