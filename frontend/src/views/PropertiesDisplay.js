import React from 'react'
import { connect } from "react-redux"
import "assets/PropDisplay.css"


import {
    Col,
    Row,
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button, Badge
} from "reactstrap"

class PropertiesDisplay extends React.Component {

    state = {
        currentImg: 0
    }

    render() {
        return <React.Fragment>
            <Row className="d-flex justify-content-center">
                {this.renderList()}
            </Row>
        </React.Fragment>
    }

    renderList() {
        return this.props.propList.map(p => {
            return this.renderPropertie(p)
        })
    }

    renderPropertie(propertie) {



        return <Col sm={12} md={6} lg={4} xl={3} key={propertie.realEstateId}>
            <Card className="mb-3">
                <CardImg top width="100%" src={propertie.data.picture[this.state.currentImg]} alt="Card image cap" style={{ cursor: "pointer" }} onClick={() => { this.renderOtherImg(propertie) }} />
                <CardBody className="cardImg">
                    <CardText className="text-center font-italic">(Click on image to view more)</CardText>
                    <CardTitle tag="h5">{propertie.data.name}</CardTitle>
                    <CardSubtitle tag="h6" className="mb-2 text-muted">{propertie.data.address}</CardSubtitle>
                    <CardText>{propertie.data.desc}</CardText>
                    <Badge className="badgesty" color="success"><h5>{propertie.price / 1000000000000000000} ETH</h5></Badge>
                    {this.props.botChild(propertie)}
                </CardBody>
            </Card>
        </Col>
    }

    renderOtherImg(propertie) {
        let temp = this.state.currentImg + 1
        if (temp >= propertie.data.picture.length) temp = 0
        this.setState({
            currentImg: temp
        })

    }

}

export default PropertiesDisplay