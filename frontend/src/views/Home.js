import React from 'react';
import 'assets/Home.css';
import { connect } from "react-redux"


import {
  Row,
  Col,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText,
  Container
} from "reactstrap"

import {
  Link
} from "react-router-dom";

import Web3Component from "component/Web3Component"



class Home extends React.Component {


  state = {
    isOpen: false
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }



  render() {
    return <React.Fragment>
      <Web3Component />
      <div>
        <Navbar color="dark" dark expand="md">
          <NavbarBrand href="/">SupEstate</NavbarBrand>
          <NavbarToggler onClick={this.toggle.bind(this)} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="mr-auto" navbar>
              <NavItem>
                <Link to="/myprops"><NavLink>Your Properties</NavLink></Link>
              </NavItem>
              <NavItem>
                <Link to="/buy"><NavLink>Buy</NavLink></Link>
              </NavItem>
              <NavItem>
                <Link to="/admin"><NavLink>Admin</NavLink></Link>
              </NavItem>
            </Nav>
          </Collapse>
          <NavbarText> {this.renderEth()}</NavbarText>
        </Navbar>
      </div>
      <Container>
       
        <div className="mainTitle text-back text-center">
          <Row>
            <Col>
              <h1 style={{ color: "#a4b1a1" }} className="font-weight-bold">SUPEstate</h1>
            </Col>
          </Row>
          <Row>
            <Col>
              <h5 style={{ color: "#a4b1a1" }} className="font-italic">Powered by eth</h5>
            </Col>
          </Row>
        </div>
      </Container>

    </React.Fragment>
  }

  renderEth() {
   
    return <div className="text-white ethDisplay text-center">
     ETH: {this.props.balance} ||  Address: {this.props.address} 
    </div>
  }
}

const mapStateToProps = state => {
  return {
    address: state.web3data.address,
    balance: state.web3data.balance,
    contractName: state.web3data.contractName,
  }
}

export default connect(mapStateToProps)(
  Home
)
