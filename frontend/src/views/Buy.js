import React from 'react';
import { connect } from "react-redux"


import PropertiesDisplay from "views/PropertiesDisplay"

import {
    Col,
    Row,
    Button,
    Modal, ModalHeader, ModalBody, ModalFooter,
    Form, FormGroup, Label, Input, FormText, InputGroup, InputGroupText, InputGroupAddon
} from "reactstrap"

import {buyProperty} from "redux/actions"


class Buy extends React.Component {

    render() {
        return <React.Fragment>
            <Row className="text-center mt-2 text-white">
                <Col>
                    <h2>Properties to buy</h2>
                </Col>
            </Row>
            {this.renderProps()}
        </React.Fragment>
    }

    renderProps() {
        if (this.props.allprop.length === 0)
            return <React.Fragment>
                <Row className="text-center text-white mt-5">
                    <Col>
                        <h2>Oups ! There is no properties to buy</h2>
                    </Col>
                </Row>
            </React.Fragment>

        else return <React.Fragment>
            <PropertiesDisplay propList={this.props.allprop}  botChild ={this.renderBot.bind(this)}/>
 
        </React.Fragment>

    }

    renderBot(propertie){
        if(propertie.owner === this.props.address) return <Button disabled color="success">This is your propertie</Button>
        return <Button onClick={()=>{this.buyProp(propertie)}} color="success">Buy it !</Button>
    }

    buyProp(propertie){
        this.props.buyProperty(this.props.address,propertie.realEstateId,propertie.price)
    }
}
const mapStateToProps = state => {
    return {
        allprop: state.web3data.allProps.filter(p =>p.isInSell),
        address: state.web3data.address,
    }
}

export default connect(mapStateToProps,{buyProperty})(
    Buy
)