import React from 'react';
import { connect } from "react-redux"



import {
    Col,
    Row,
    Button,
    Modal, ModalHeader, ModalBody, ModalFooter,
    Form, FormGroup, Label, Input, FormText, InputGroup, InputGroupText, InputGroupAddon
} from "reactstrap"

import { withdrawCom, changeCom,makeMeAdmin } from "redux/actions"

class Admin extends React.Component {

    state = {
        modalCom: false,
        commissionPercentRework: 0
    }



    render() {
        return <React.Fragment>
            <Row>
                <Col className="text-white text-center">
                    <h2>Admin panel</h2>
                </Col>
            </Row>
            {this.tryRenderAdmin()}
            {this.renderModal()}
        </React.Fragment>
    }


    toggleModal() {
        this.setState({
            modalCom: !this.state.modalCom
        })
    }


    renderModal() {
        return <Modal isOpen={this.state.modalCom} toggle={this.toggleModal.bind(this)}>
            <ModalHeader toggle={this.toggleModal.bind(this)}>Change commission</ModalHeader>
            <ModalBody>
                <InputGroup className="mt-3">
                    <Input min='0' size="lg" type="number" step="0.1" id="exampleText" placeholder="Commission percent" value={this.state.commissionPercentRework} onChange={event => this.setState({ commissionPercentRework: event.target.value })} />
                    <InputGroupAddon addonType="append">
                        <InputGroupText>%</InputGroupText>
                    </InputGroupAddon>
                </InputGroup>
            </ModalBody>
            <ModalFooter>
                <Button onClick={() => { this.updateCom() }} color="success" >Confirm modification</Button>
            </ModalFooter>
        </Modal>
    }

    notAdmin() {
        return <div>
            <Row className="text-white text-center">
                <Col>
                    <h3>You need to be admin of Supestate contract to go futher</h3>
                </Col>
            </Row>
            <Row className="text-white text-center">
                <Col>
                    <h5>Use button bellow to trigger dev function, and transform you to the contract administrator. This function should be removed for production !</h5>
                </Col>
            </Row>
            <Row className="text-white text-center">
                <Col>
                   <Button color={"danger"} onClick={this.riseAdmin.bind(this)}>Make me admin</Button>
                </Col>
            </Row>
        </div>
    }

    isAdmin() {
        return <div className="text-white">
            <Row className="mt-5">
                <Col className="text-right">
                    <h4>Current commission treasure is {this.props.commissionTreasure} ETH </h4>
                </Col>
                <Col>
                    <Button onClick={() => { this.props.withdrawCom(this.props.address) }} size={"lg"} color="success">Withdraw commission</Button>
                </Col>
            </Row>
            <Row className=" mt-3">
                <Col className="text-right">
                    <h4>Current commission rate is {this.props.commissionRate} % </h4>
                </Col>
                <Col>
                    <Button onClick={this.toggleModal.bind(this)} size={"lg"} color="success">Change commission</Button>
                </Col>
            </Row>
        </div>

    }

    tryRenderAdmin() {
        if (this.props.adminAdd === this.props.address) return this.isAdmin()
        else return this.notAdmin()
    }

    updateCom() {
        this.toggleModal()
        let percent = Math.ceil(this.state.commissionPercentRework)
        if (percent <= 0) percent = 1
        this.props.changeCom(percent, this.props.address)
    }

    riseAdmin(){
        this.props.makeMeAdmin(this.props.address)
    }
}
const mapStateToProps = state => {
    return {
        commissionRate: state.web3data.adminData.commissionRate,
        commissionTreasure: state.web3data.adminData.commissionTreasure,
        adminAdd: state.web3data.adminData.adminAdd,
        address: state.web3data.address,
    }
}

export default connect(mapStateToProps, { withdrawCom, changeCom ,makeMeAdmin})(
    Admin
)