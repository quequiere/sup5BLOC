import React from "react"
import { connect } from "react-redux"


import PropertiesDisplay from "views/PropertiesDisplay"

import {
    Col,
    Row,
    Button,
    Modal, ModalHeader, ModalBody, ModalFooter,
    Form, FormGroup, Label, Input, FormText, InputGroup, InputGroupText, InputGroupAddon
} from "reactstrap"

import "assets/yourprop.css"

import MultipleValueTextInput from 'react-multivalue-text-input';

import { postRealEstateSell, toggleSellStatus,changeDataProperty } from "redux/actions"

import { BiEditAlt } from "react-icons/bi"

class YourProperties extends React.Component {

    state = {
        sellModal: false,
        propname: '',
        propaddress: '',
        desc: '',
        pictures: [],
        ethprice: '',
        currentEditProp: undefined
    }

    toggleModalSell() {
        this.setState({
            sellModal: !this.state.sellModal,
            propname: '',
            propaddress: '',
            desc: '',
            pictures: [],
            ethprice: '',
            currentEditProp: undefined
        })
    }

    toggleModalSellModify(propertie) {
        console.log(propertie.data.picture)
        this.setState({
            sellModal: !this.state.sellModal,
            propname: propertie.data.name,
            propaddress: propertie.data.address,
            desc: propertie.data.desc,
            pictures: propertie.data.picture,
            ethprice: propertie.price / 1000000000000000000,
            currentEditProp: propertie
        })
    }


    render() {
        return <React.Fragment>
            <Row className="text-center mt-2">
                <Col>
                    <Button color="primary" onClick={this.toggleModalSell.bind(this)}>Add realestate to sell</Button>
                </Col>
            </Row>
            {this.renderProps()}

            {this.renderModal()}
        </React.Fragment>
    }

    renderProps() {
        if (this.props.ownedProp.length === 0)
            return <React.Fragment>
                <Row className="text-center text-white mt-5">
                    <Col>
                        <h2>Oups ! You haven't put in sell properties yet.</h2>
                    </Col>
                </Row>
            </React.Fragment>

        else return <React.Fragment>
            <Row className="text-center mt-5">
                <Col>
                    <h2 className="text-white">Your properties:</h2>
                </Col>
            </Row>
            <PropertiesDisplay propList={this.props.ownedProp} botChild={this.renderPropBut.bind(this)} />


        </React.Fragment>

    }

    renderPropBut(propertie) {
        return <div>
            {this.renderPropButPart(propertie)}
            <div style={{ position: "absolute", top: "10px", right: "10px", backgroundColor: "purple", borderRadius: "7px", cursor: "pointer" }}>
                <BiEditAlt color={"white"} size={35} onClick={() => this.toggleModalSellModify(propertie)} />
            </div>
        </div>
    }

    renderPropButPart(propertie) {
        if (propertie.isInSell) return <Button color="danger" onClick={() => { this.changeSellStatut(false, propertie) }}>Cancel sell</Button>
        else return <Button color="primary" onClick={() => { this.changeSellStatut(true, propertie) }}>Put in sell</Button>
    }


    renderModal() {
        return <Modal isOpen={this.state.sellModal} toggle={this.toggleModalSell.bind(this)}>
            <ModalHeader toggle={this.toggleModalSell.bind(this)}>Create sell</ModalHeader>
            <ModalBody>
                <InputGroup>
                    <Input placeholder="Property name" value={this.state.propname} onChange={event => this.setState({ propname: event.target.value })} />
                </InputGroup>
                <InputGroup className="mt-3">
                    <Input placeholder="Property address" value={this.state.propaddress} onChange={event => this.setState({ propaddress: event.target.value })} />
                </InputGroup>
                <InputGroup className="mt-3">
                    <Input type="textarea" id="exampleText" placeholder="Add more informations about property here" value={this.state.desc} onChange={event => this.setState({ desc: event.target.value })} />
                </InputGroup>
                <InputGroup className="mt-3">
                    <Input size="lg" type="number" step="0.1" id="exampleText" placeholder="ETH price" value={this.state.ethprice} onChange={event => this.setState({ ethprice: event.target.value })} />
                    <InputGroupAddon addonType="append">
                        <InputGroupText>ETH</InputGroupText>
                    </InputGroupAddon>
                </InputGroup>
                {this.renderPrice()}
                <div className="listinput" >
                    <MultipleValueTextInput
                        values={this.state.pictures}
                        onItemAdded={(item, allItems) => this.setState({
                            pictures: allItems
                        })}
                        onItemDeleted={(item, allItems) => this.setState({
                            pictures: allItems
                        })}
                        label={<div className="mb-2">Pictures URL</div>}
                        name="item-input"
                        placeholder="Write picture URL then press ENTER"
                        deleteButton={<span><Button color={"danger"}>X</Button><br /></span>}
                    />
                </div>

            </ModalBody>
            <ModalFooter>
                <Button color="success" onClick={this.confirmSell.bind(this)} >Confirm sell</Button>
            </ModalFooter>
        </Modal>

    }

    renderPrice() {
        //eth actual price
        let priceCompute = this.state.ethprice * 1700
        return <div className="text-center">
            ~ Arround {priceCompute} $
        </div>
    }

    confirmSell() {
        let priceToWei = 1000000000000000000 * this.state.ethprice

        let data = {
            name: this.state.propname,
            address: this.state.propaddress,
            desc: this.state.desc,
            picture: this.state.pictures
        }


        let finalData = btoa(JSON.stringify(data))

        //If we are editing special send
        if (this.state.currentEditProp != undefined)
            this.props.changeDataProperty(finalData, String(priceToWei), this.props.address,this.state.currentEditProp.realEstateId)
        else
            this.props.postRealEstateSell(finalData, String(priceToWei), this.props.address)


        this.toggleModalSell()

    }


    changeSellStatut(isSelling, propertie) {
        this.props.toggleSellStatus(isSelling, this.props.address, propertie.realEstateId)
    }


}

const mapStateToProps = state => {
    return {
        ownedProp: state.web3data.allProps.filter(p => { return p.owner === state.web3data.address }),
        address: state.web3data.address,
    }
}

export default connect(mapStateToProps, { postRealEstateSell, toggleSellStatus,changeDataProperty })(
    YourProperties
)