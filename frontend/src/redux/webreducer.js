//Example

const exampleProp = {
  owner: '1234',
  realEstateId: '1',
  isInSell: true,
  price: 0,
  data: {
    name: 'hello',
    address: '78 azaz zaz 75003 PARIS',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla id sapien',
    picture: ["http://url1.com/toto.png", "http://url1.com/toto.png"]
  },
  adminData: {
    commissionRate: 0,
    commissionTreasure: 0
  }
}




const initialState = {
  balance: "--",
  contractName: "",
  address: "",
  allProps: [],
  adminData: {
    commissionRate: 0,
    commissionTreasure: 0,
    adminAdd: "emptyvalue"
  }
}

const serversReducer = (state = initialState, action) => {

  switch (action.type) {

    case "REALOAD_ADMIN_DATA":
      return {
        ...state,
        adminData: {
          commissionRate : action.data.rate,
          commissionTreasure : action.data.treasure,
          adminAdd : action.data.adminAdd
        },
      }

    case "ADDRESS_UPDATE":
      return {
        ...state,
        address: action.data,
      }

    case "BALANCE_UPDATE":
      return {
        ...state,
        balance: action.data,
      }

    case "CONTRACT_NAME":
      return {
        ...state,
        contractName: action.data,
      }

    case "RELOAD_ALL_PROPS":
      return {
        ...state,
        allProps: action.data,
      }

    default: return state
  }

}

export default serversReducer