

const axios = require('axios').default;


export const loadAddress = () => {

    return dispatch => {
        window.web3.eth.getAccounts()
            .then(e =>
                dispatch({
                    type: "ADDRESS_UPDATE",
                    data: e[0]
                })
            )
    }
}


export const updatedBalance = () => {

    return dispatch => {

        window.web3.eth.getAccounts()
            .then(e => window.web3.eth.getBalance(e[0]))
            .then(e => dispatch({
                type: "BALANCE_UPDATE",
                data: e / 1000000000000000000
            }))

    }
}

export const updateContractName = () => {

    return dispatch => {
        window.REContract.methods.name().call().then(e =>
            dispatch({
                type: "CONTRACT_NAME",
                data: e
            })
        )
    }
}

export const postRealEstateSell = (data, weiPrice, address) => {
    return dispatch => {
        window.REContract.methods.declareSell(data, weiPrice).send({ from: address })
        return { dispatch: false }
    }
}





export const toggleSellStatus = (sellStatus, address, realEstateId) =>  (dispatch) => {
    window.REContract.methods.setInSell(realEstateId,sellStatus).send({ from: address })
}



export const buyProperty = (address, realEstateId,price) =>  (dispatch) => {
    window.REContract.methods.buyRealEstate(realEstateId).send({ from: address , value: price})
}

export const changeDataProperty = (data,price,address,realEstateId) =>  (dispatch) => {
    window.REContract.methods.updateURIRef(realEstateId,data,price).send({ from: address })
}


export const loadAdminData = () => async (dispatch) => {

    let treasure = await window.REContract.methods.getComTreasure().call()
    let rate = await window.REContract.methods.getComRate().call()
    let adminAdd = await window.REContract.methods.getContractOwner().call()

    return dispatch({
        type: "REALOAD_ADMIN_DATA",
        data: {
            treasure: treasure/1000000000000000000,
            rate: rate,
            adminAdd: adminAdd
        }
    })
}


export const withdrawCom = (address) =>  (dispatch) => {
    window.REContract.methods.withdrawCommission().send({ from: address })
}

export const changeCom = (percent,address) =>  (dispatch) => {
    window.REContract.methods.changeCommission(percent).send({ from: address })
}

export const makeMeAdmin = (address) =>  (dispatch) => {
    window.REContract.methods.devToolMakeMeAdmin().send({ from: address })
}


export const loadAllTokens = () => async (dispatch) => {

    let allTokens = await window.REContract.methods.getAllTokens().call()
    let allProperties = await Promise.all(
        allTokens.map(async (tId) => {
            try{
                let owner = await window.REContract.methods.ownerOf(tId).call()
                let price = await window.REContract.methods.getPrice(tId).call()
                let isInSell = await window.REContract.methods.isInSell(tId).call()
    
                let dataStored = await window.REContract.methods.tokenURI(tId).call()
                dataStored = JSON.parse(atob(dataStored))
                return {
                    realEstateId:tId,
                    owner: owner,
                    isInSell: isInSell,
                    price: price,
                    data : dataStored
                }
            }catch(e){
               //We handle here if propertie data are not compatible with website
               return null
            }
        })
    )

    allProperties = allProperties.filter(p => { return p !== null })

    return dispatch({
        type: "RELOAD_ALL_PROPS",
        data: allProperties
    })
}
