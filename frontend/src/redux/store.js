import { configureStore } from '@reduxjs/toolkit';
import webreducer from 'redux/webreducer';

export default configureStore({
  reducer: {
    web3data: webreducer,
  },
});
