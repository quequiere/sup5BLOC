# SUPEstate - 5BLOC

LARIBIERE Bruno - 298291 - bruno.laribiere@supinfo.com

*Pour mettre en place le site merci de vous référer à la documentation technique*

### Publication 

Le contrat est publié **ET vérifié** sur l'URL suivante:

https://rinkeby.etherscan.io/address/0x73c6Fa2cC2B2998f47569418AEd6fb5115D44dEE



## Documentation utilisateur

### Métamask

Une fois arrivé sur le site web depuis son adresse locale: http://localhost:3000/ , vous devrez confirmer la connexion avec Métamask. Sans cela il ne vous sera pas possible de consulter le site

<img src=".\screenshot\metamask.jpg" style="zoom:50%;" />

### Contrat

Veuillez être connecté à la block chain test Rinkeby avec metamask

L'adresse actuelle du contrat est

```
0x73c6Fa2cC2B2998f47569418AEd6fb5115D44dEE
```

### Home page

Vous êtes à présent sur la page d'accueil, utilisez la barre de navigation en haut de l'écran. Par ailleurs vous devriez voir votre adresse ETH et votre balance en haut à droite.

Vous pouvez directement switcher de compte avec Métamask, le site prendra en compte le changement.

Le site étant dynamique, vous n'avez pas besoin de rafraichir la page du navigateur. Veuillez simplement attendre que vos transactions se confirment, et le contenu sera automatiquement rafraichi

<img src=".\screenshot\Home page.jpg" style="zoom:50%;" />

### Mise en vente

Dans l'onglet "your properties" vous verrez apparaitre l'ensemble de vos propriétés. Si vous n'en n'avez pas vous pouvez en créer avec le bouton associé. Les images sont des urls à renseigner. Voici par exemple un site ou vous pourrez des images très simplement pour vos tests 

https://homepages.cae.wisc.edu/~ece533/images/

<img src=".\screenshot\CreateSell.jpg" style="zoom:67%;" />

### Vos propriétés

L'ensemble de vos propriétés sont affichés ici. Vous pouvez modifier leur données à n'importe quel moment. Mais aussi de les mettre ou retirer de la vente.

<img src=".\screenshot\CurrentPro.jpg" style="zoom:50%;" />

### Acheter

Vous pouvez acheter les propriétés disponibles dans l'onglet buy. Il vous faut donc forcément un second compte pour faire ce test car nous ne pourrez pas acheter vos propres propriétés.

Une fois acheté, la propriété vous appartient et est retiré de la vente. Vous pouvez la remettre en vente via l'onglet Your properties

<img src=".\screenshot\BuyOnglet.jpg" style="zoom:50%;" />

### Panneau administrateur

Seulement si vous êtes administrateur du contrat, et donc si l'adresse qui a publiée le contrat est celle actuellement dans Métamask. Vous pourrez changer la commission et retirer les frais.

Si vous n'êtes pas admin, un bouton spécialement pour les tests a été mis en place afin de vous permettre d'être administrateur.

<img src=".\screenshot\Adminpan.jpg" style="zoom:50%;" />