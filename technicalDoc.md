# SUPEstate - 5BLOC

LARIBIERE Bruno - 298291 - bruno.laribiere@supinfo.com



## Documentation technique

### Démarrer le site web

- Rendez vous à la racine du projet et exécuter la commande suivante:

```
docker-compose up -d
```

Le site est disponible sur votre port local 3000 http://localhost:3000/

Pour éteindre le site:

```
docker-compose down
```



Le contrat étant hébergé sur rinkeby test network, vous n'avez rien besoin de faire par rapport au contrat.